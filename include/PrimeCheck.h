#pragma once
#include <cstdint>
namespace primality
{
    class PrimeCheck
    {
        public:
            ///The parent class of all of my prime number calculator code.
            virtual bool isPrime(uint64_t value __attribute__((unused))) {return false;};

    };
}