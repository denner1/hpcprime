#pragma once
#include "PrimeCheck.h"
#include <gmp.h>
namespace primality
{
    class GmpPrimeCheck : public primality::PrimeCheck
    {
        public:
            ///This uses the GNUMP probab_prime test. Note that this uses several math tests that
            ///have a very small chance (4^-50) of identifying a false prime. see https://gmplib.org/manual/Number-Theoretic-Functions#Number-Theoretic-Functions
            ///For more info
            bool isPrime(uint64_t value) override;
    };
}