#pragma once
#include "PrimeCheck.h"
#include <unordered_set>

namespace primality
{
    class BoostPrimeCheck : public primality::PrimeCheck
    {
        public:
            BoostPrimeCheck();
            ///uses the boost prime library that stores the first 10000 primes in a lookup table.
            ///note that it will not identify any primes larger than LargestPrime as that is the end of the table.
            ///This is rather fast though for the numbers small enough to getaway with it...
            bool isPrime(uint64_t value) override; 
            ///Returns the largest prime that boost knows about.
            uint64_t LargestPrime();
        private:
            std::unordered_set<uint32_t> primes;
    };
}
