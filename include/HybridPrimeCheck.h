#pragma once
#include "PrimeCheck.h"
#include "BoostPrimeCheck.h"
#include "GmpPrimeCheck.h"
namespace primality
{
    class HybridPrimeCheck : public primality::PrimeCheck
    {
        public:
            ///This uses the GNUMP probab_prime test and Boost Prime Tests (if below boost's cutoff). Note that this uses several math tests that
            ///have a very small chance (4^-50) of identifying a false prime. see https://gmplib.org/manual/Number-Theoretic-Functions#Number-Theoretic-Functions
            ///For more info
            bool isPrime(uint64_t value) override;
        private:
            BoostPrimeCheck boostp;
            GmpPrimeCheck gmpp;
            uint64_t maxboost= this->boostp.LargestPrime();
    };
}