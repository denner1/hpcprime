#pragma once
#include<vector>
#include <omp.h>
#include "PrimeCheck.h"
#include <boost/log/trivial.hpp>
#include <boost/timer/timer.hpp>

std::vector<uint64_t> primes(uint64_t start, uint64_t stop, int threadcount, primality::PrimeCheck& primeCheck)
{
    boost::timer::cpu_timer time;
    BOOST_LOG_TRIVIAL(info)  << "Calculating primes from " << start << " to " << stop << " on " << threadcount << " threads.";
    std::vector<uint64_t> collector;
    //note that this is using static scheduling, round robin chunks of numbers will be assigned equaly between the threads. (this is the default behavior)
    // we could also do dynamic or guided but is not really needed for this problem.
    omp_set_num_threads(threadcount);
    #pragma omp parallel
    {
        std::vector<uint64_t> localCollector;
        uint64_t workcount=0;
        #pragma omp for  
        for (size_t i = start; i <= stop; i++)
        {
            if (primeCheck.isPrime(i))
            {
                    localCollector.push_back(i);
            }
            workcount++;
        }
        BOOST_LOG_TRIVIAL(info) << localCollector.size() << " Items found by Thread of " << workcount << " counted."; 
        #pragma omp critical
        {
            collector.insert(collector.end(),localCollector.begin(),localCollector.end());
        }
    }
    time.stop();
    BOOST_LOG_TRIVIAL(info) << "We finished the primes calculations. This resulted in finding " << collector.size() << " primes.";
    BOOST_LOG_TRIVIAL(info) << "This took: " << time.format(3);
    return collector;
};