#pragma once
#include "PrimeCheck.h"
#include <cmath>
namespace primality
{
    class NaivePrimeCheck : public primality::PrimeCheck
    {
        public:
            ///The simple case of 2 to sqrt(n). Performance will be less than ideal.
            bool isPrime(uint64_t value) override;
    };
}