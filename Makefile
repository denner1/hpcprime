CXX		  := g++
CXX_FLAGS := -Wall -Wextra -std=c++17 -ggdb -O3 -fopenmp -lgomp -lboost_timer -lboost_log -DBOOST_LOG_DYN_LINK -lboost_log_setup -lpthread -lboost_system -lboost_program_options -lgmp
CXX_TEST_FLAGS := -lboost_unit_test_framework

BIN		:= bin
SRC		:= src
INCLUDE	:= include
LIB		:= lib
TEST	:= test
TESTINCLUDE := $(filter-out src/main.cpp, $(wildcard src/*.cpp))

LIBRARIES	:=
EXECUTABLE	:= hpcPrime
TESTEXE		:= hpcPrimeTest


all: $(BIN)/$(EXECUTABLE)

run: clean all
	clear
	./$(BIN)/$(EXECUTABLE)

test: $(BIN)/$(TESTEXE)

$(BIN)/$(EXECUTABLE): $(SRC)/*.cpp
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)

$(BIN)/$(TESTEXE): $(TEST)/*.cpp $(TESTINCLUDE)
	$(CXX) $(CXX_FLAGS) $(CXX_TEST_FLAGS) -I$(INCLUDE) -L$(LIB) $^ -o $@ $(LIBRARIES)
clean:
	-rm $(BIN)/*
