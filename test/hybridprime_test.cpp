#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE Main
#endif
#include <boost/test/unit_test.hpp>
#include "HybridPrimeCheck.h"

BOOST_AUTO_TEST_SUITE(HybridPrimeCheck_suite)

BOOST_AUTO_TEST_CASE(TestLowPrime)
{
    primality::HybridPrimeCheck p;
    BOOST_CHECK(p.isPrime(3));
}

BOOST_AUTO_TEST_CASE(TestLowNotPrime)
{
    primality::HybridPrimeCheck p;
    BOOST_CHECK(!p.isPrime(4));
}
BOOST_AUTO_TEST_CASE(TestHighNotPrime)
{
    primality::HybridPrimeCheck p;
    BOOST_CHECK(!p.isPrime(961748943));
}
BOOST_AUTO_TEST_CASE(TestHighPrime)
{
    primality::HybridPrimeCheck p;
    BOOST_CHECK(p.isPrime(961748947));
}
BOOST_AUTO_TEST_SUITE_END()
