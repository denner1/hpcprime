#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE Main
#endif
#include <boost/test/unit_test.hpp>
#include "primes.h"
#include "BoostPrimeCheck.h"
#include "GmpPrimeCheck.h"

BOOST_AUTO_TEST_SUITE(primesFunction_suite)

BOOST_AUTO_TEST_CASE(SingleThreadPrimeTest)
{
    primality::BoostPrimeCheck p;
    auto collector = primes(1, 100,  1, p);
    BOOST_CHECK(collector.size()>1);
}

BOOST_AUTO_TEST_CASE(MultiThreadPrimeTest)
{
    primality::BoostPrimeCheck p;
    auto collector = primes(1, 100,  8, p);
    BOOST_CHECK(collector.size()>1);
}
BOOST_AUTO_TEST_CASE(TestHighNotPrime)
{
    primality::GmpPrimeCheck p;
    auto collector = primes(961748941, 961748943, 8, p);
    BOOST_CHECK(collector.size() == 1);
}
BOOST_AUTO_TEST_CASE(StrangeValueTest)
{
    primality::GmpPrimeCheck p;
    auto collector = primes(0, 0,  111, p);
    BOOST_CHECK(collector.size()==0);
}
BOOST_AUTO_TEST_SUITE_END()
