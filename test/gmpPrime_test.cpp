#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE Main
#endif
#include <boost/test/unit_test.hpp>
#include "GmpPrimeCheck.h"

BOOST_AUTO_TEST_SUITE(GmpPrimeCheck_suite)

BOOST_AUTO_TEST_CASE(TestLowPrime)
{
    primality::GmpPrimeCheck p;
    BOOST_CHECK(p.isPrime(3));
}

BOOST_AUTO_TEST_CASE(TestLowNotPrime)
{
    primality::GmpPrimeCheck p;
    BOOST_CHECK(!p.isPrime(4));
}
BOOST_AUTO_TEST_CASE(TestHighNotPrime)
{
    primality::GmpPrimeCheck p;
    BOOST_CHECK(!p.isPrime(961748943));
}
BOOST_AUTO_TEST_CASE(TestHighPrime)
{
    primality::GmpPrimeCheck p;
    BOOST_CHECK(p.isPrime(961748947));
}
BOOST_AUTO_TEST_SUITE_END()
