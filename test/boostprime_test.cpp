#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#   define BOOST_TEST_MODULE Main
#endif
#include <boost/test/unit_test.hpp>
#include "BoostPrimeCheck.h"

BOOST_AUTO_TEST_SUITE(boostPrimeCheck_suite)

BOOST_AUTO_TEST_CASE(TestTwoPrime)
{
    primality::BoostPrimeCheck p;
    BOOST_CHECK(p.isPrime(2));
}


BOOST_AUTO_TEST_CASE(TestLowPrime)
{
    primality::BoostPrimeCheck p;
    BOOST_CHECK(p.isPrime(3));
}

BOOST_AUTO_TEST_CASE(TestLowNotPrime)
{
    primality::BoostPrimeCheck p;
    BOOST_CHECK(!p.isPrime(9));
}
BOOST_AUTO_TEST_CASE(TestHighestPrime)
{
    //This tests the largest prime that boost knows about since it is just a table of numbers
    primality::BoostPrimeCheck p;
    BOOST_CHECK((p.isPrime(p.LargestPrime())));
}
BOOST_AUTO_TEST_CASE(TestHighestNotPrime)
{
    //Check to see if we know about a non prime at the end of the range that boost knows about
    primality::BoostPrimeCheck p;
    BOOST_CHECK(!(p.isPrime(p.LargestPrime()-1)));
}
BOOST_AUTO_TEST_SUITE_END()
