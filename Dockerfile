FROM debian:latest as baseimage
RUN apt-get update; apt-get -y install libgmp-dev libomp-dev libboost-all-dev

FROM baseimage as builderimage
RUN apt-get update; apt-get -y install build-essential

FROM builderimage as builder
WORKDIR /work
COPY . .
RUN make all

FROM builderimage as test
WORKDIR /work
COPY . .
RUN make test
RUN ./bin/hpcPrimeTest

FROM baseimage as final
RUN apt-get install -y gdb
WORKDIR /HPCPrime
RUN chmod a+rw .
RUN adduser --disabled-password --gecos '' sce
USER sce
COPY --from=builder /work/bin/. .
#ENTRYPOINT "/HPCPrime/./hpcPrime"
#CMD [hpcPrime --help]

