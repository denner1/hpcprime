#include "NaivePrimeCheck.h"
using namespace primality;

bool NaivePrimeCheck::isPrime(uint64_t n)
{
    if (n<2) 
        return false;
    if (n<4) //if 2 or 3 we know it is prime
        return true;
    if (n%2==0) //if even not prime
        return false;
    uint64_t imax = std::sqrt(n);
    for (uint64_t i = 3; i <= imax; i += 2)
    {
        if (n % i == 0) 
            return false;
    }
    return true;
}