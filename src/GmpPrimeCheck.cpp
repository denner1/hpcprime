#include "GmpPrimeCheck.h"
#include <boost/log/trivial.hpp>

bool primality::GmpPrimeCheck::isPrime(uint64_t p)
{
    mpz_t n;  //This is slightly less performant but is thread safe as gmp requires you to use their objects and clean up after them
    mpz_init(n); 
    mpz_set_ui(n,p);
    auto res=mpz_probab_prime_p(n,50);
    mpz_clear(n);
    return res != 0; //note that there is an insanely small (4^-50) chance that we could be wrong here. See https://gmplib.org/manual/Number-Theoretic-Functions#Number-Theoretic-Functions
}
