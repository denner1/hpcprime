#include <boost/program_options.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/core.hpp>
#include <iostream>
#include <exception>
#include <algorithm> // for copy
#include <iterator> // for ostream_iterator
#include "BoostPrimeCheck.h"
#include "GmpPrimeCheck.h"
#include "NaivePrimeCheck.h"
#include "PrimeCheck.h"
#include "primes.h"
#include "HybridPrimeCheck.h"
#include <boost/algorithm/string.hpp>
#include <string>

namespace opt = boost::program_options;
namespace blog = boost::log;

int main(int argc, char *argv[]) 
{
    blog::core::get()->set_filter( blog::trivial::severity >= blog::trivial::error);  //by default quiet most of the log events
   
    //first we parse command line arguments
    opt::options_description desc("HpcPrime, a coding problem for a corteva interview \n All options");
    desc.add_options()
    ("start,s", opt::value<uint64_t>()->default_value(0), "The number to start testing for primes at (inclusive)")
    ("end,e", opt::value<uint64_t>()->default_value(0), "The number to stop testing for primes at (inclusive)")
    ("threads,t", opt::value<uint>()->default_value(1), "How many threads to run in parallel (default 1)")
    ("model,m", opt::value<std::string>()->default_value("gmp"), "Which prime test to use, options are hybrid, gmp, boost, or naive.")
    ("verbose,v", "Turns on additional logging")
    ("help,h", "produces this help message");
    opt::variables_map vm;
    opt::store(opt::parse_command_line(argc, argv, desc), vm);
    opt::notify(vm);

    //now that command line args are parsed lets check for a few flagged options to set up
    if (vm.count("help") || argc == 1) //if -h or no args are given 
    {
        //display help information and exit
        std::cout << desc << "\n";
        return 1;
    }
    if (vm.count("verbose"))
    {
        //move logging to verbose mode. Actually we are going for info level since I didn't log any lower messages.
        blog::core::get()->set_filter( blog::trivial::severity >= blog::trivial::info);
    }

    if (vm["start"].as<uint64_t>() > vm["end"].as<uint64_t>())
    {
        BOOST_LOG_TRIVIAL(fatal) << "Start value must be less than or equal to end value. Try flipping them.";
        return 1;
    }

    //figure out which prime test we should use
    primality::PrimeCheck* primeTest;
    primality::HybridPrimeCheck hybrid;
    primality::GmpPrimeCheck gmpPrime;
    primality::BoostPrimeCheck boostPrime;
    primality::NaivePrimeCheck naivePrime;
    std::string primeFlag=vm["model"].as<std::string>(); //this is outside the try to give a meaningful error message if you give an non string
    try
    {
        //figure out what model the user wants to use for testing as a prime
        boost::algorithm::to_lower(primeFlag); 
        if (primeFlag=="gmp")
        {
            BOOST_LOG_TRIVIAL(info) << "You chose GMP as a prime test";
            primeTest= &gmpPrime;
        }
        else if (primeFlag=="hybrid")
        {
            BOOST_LOG_TRIVIAL(info) << "You chose a hybrid model as a prime test, this combines boost and GMP.";
            primeTest = &hybrid;
        }
        else if (primeFlag=="boost")
        {
            BOOST_LOG_TRIVIAL(info) << "You chose BOOST as a prime test";
            if (boostPrime.LargestPrime() < vm["end"].as<uint64_t>())
            {
                BOOST_LOG_TRIVIAL(fatal) << "You are looking for primes larger than this prime test supports. Please choose a start and end smaller than " << boostPrime.LargestPrime();
                return 1;
            }
            primeTest=&boostPrime;
        }
        else if (primeFlag=="naive")
        {
            BOOST_LOG_TRIVIAL(info) << "You chose Naive as a prime test";
            primeTest=&naivePrime;
        }
        else 
        {
            BOOST_LOG_TRIVIAL(fatal) << "Unknown Model Selected accepted values are gmp, boost, and naive";
            return 1;
        }

        //now that all setup work is done lets calculate some primes.
        std::vector<uint64_t> collector = primes(vm["start"].as<uint64_t>(), vm["end"].as<uint64_t>(),  vm["threads"].as<uint>(), *primeTest);
        //and print them to cout
        std::copy(collector.begin(), collector.end(), std::ostream_iterator<uint64_t>(std::cout, "\n"));
        //optionally log out counts.
        BOOST_LOG_TRIVIAL(info) << collector.size() << " found.";

    }
    catch (std::exception& e)
    {
        BOOST_LOG_TRIVIAL(fatal) << "A unhandled exception happened This was unexpected:  " << e.what();
        return 1;
    }
    return 0;
}