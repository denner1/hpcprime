#include "BoostPrimeCheck.h"
#include <boost/math/special_functions/prime.hpp>
#include <boost/log/trivial.hpp>

primality::BoostPrimeCheck::BoostPrimeCheck()
{
    for (size_t i = 0; i < boost::math::max_prime; i++)
    {
        uint32_t v = boost::math::prime(i);
        this->primes.insert(v);
    }
}
uint64_t primality::BoostPrimeCheck::LargestPrime()
{
   return boost::math::prime(boost::math::max_prime-1);
}

bool primality::BoostPrimeCheck::isPrime(uint64_t p)
{
    if (((p%2)==0) && (p>4)) //get rid of even numbers to cut down on the number of lookups
        return false;
    return (!(this->primes.find(p)==this->primes.end()));
}