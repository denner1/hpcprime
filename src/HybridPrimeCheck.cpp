#include "HybridPrimeCheck.h"
#include "GmpPrimeCheck.h"
#include "BoostPrimeCheck.h"

bool primality::HybridPrimeCheck::isPrime(uint64_t p)
{
    if (this->maxboost < p)
        return this->gmpp.isPrime(p);
    return this->boostp.isPrime(p);
}